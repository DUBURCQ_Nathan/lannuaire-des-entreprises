<?php
/**
 * Plugin Name: Plugoff
 * Description: Plugoff le plugin le plus attendu de l'année. Essayez le maintenant !
 * Plugin URI: https://black-mirror.fr/
 * Author: black-mirror.fr
 * Version: 1.0
 * Author URI: https://black-mirror.fr/
 *
 * Text Domain: plugoff
 *
 * @package Plugoff
 * @category Core
 *
 * Plugoff is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Plugoff is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 */

function raynaud() {
    global $wpdb;
    $results = $wpdb->get_results("SELECT * FROM wp_annuaire");

    foreach ($results as $result){
        echo 
        "<div style='display: flex'>"
            ."<p style='padding-left: 15px'>".$result->nom_entreprise."</p>"
            ."<p style='padding-left: 15px'>".$result->localisation_entreprise."</p>"
            ."<p style='padding-left: 15px'>".$result->prenom_contact."</p>"
            ."<p style='padding-left: 15px'>".$result->nom_contact."</p>"
            ."<p style='padding-left: 15px'>".$result->mail_contact."</p>"
        ."</div>";
    }
};

add_shortcode("hello","raynaud");
/**
 * Load Plugoff textdomain.
 *
 * Load get text translate for Plugoff text domain.
 *
 * @since 1.0.0
 *
 * @return void
 */
function plugoff_load_plugin_textdomain() {
	load_plugin_textdomain( 'plugoff' );
}

/**
 * Plugoff admin notice for minimum PHP version.
 *
 * Warning when the site doesn't have the minimum required PHP version.
 *
 * @since 1.0.0
 *
 * @return void
 */
function plugoff_fail_php_version() {
	/* translators: %s: PHP version. */
	$message = sprintf( esc_html__( 'Plugoff requires PHP version %s+, plugin is currently NOT RUNNING.', 'plugoff' ), '7.0' );
	$html_message = sprintf( '<div class="error">%s</div>', wpautop( $message ) );
	echo wp_kses_post( $html_message );
}

/**
 * Plugoff admin notice for minimum WordPress version.
 *
 * Warning when the site doesn't have the minimum required WordPress version.
 *
 * @since 1.5.0
 *
 * @return void
 */
function plugoff_fail_wp_version() {
	/* translators: %s: WordPress version. */
	$message = sprintf( esc_html__( 'Plugoff requires WordPress version %s+. Because you are using an earlier version, the plugin is currently NOT RUNNING.', 'plugoff' ), '5.2' );
	$html_message = sprintf( '<div class="error">%s</div>', wpautop( $message ) );
	echo wp_kses_post( $html_message );
}
